# study-saas

## 介绍
库米运维后端学习代码

# 项目结构  
主项目下面一共有5个子项目   

![输入图片说明](https://images.gitee.com/uploads/images/2020/0113/153900_bb634aac_5245924.png "屏幕截图.png")
- study-common:通用服务
- study-common-parent: maven依赖父类
- study-facade-order: 接口层
- study-service-order: 业务实现微服务
- study-web-portal: web端微服务


## 项目需要知识点简述  

- git拉取、上推代码等的命令，对于git分支的理解
- swagger注解，swagger是什么以及swagger的使用 
- spring JPA相关注解的使用，了解JpaRepository自带的操作数据库的方法，以及自定义方法（可以结合官方文档）
- maven的基本编译

### 项目拉取示例  

git clone -b master --recursive git@gitee.com:kumiyunwii/study-saas.git

